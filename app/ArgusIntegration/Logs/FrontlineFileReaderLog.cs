﻿using ActionFramework.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArgusIntegration.Logs
{
    internal sealed class FrontlineFileReaderLog : ILogElement
    {
        private int amount = 0;
        private string fileName;
        private string filePath;
        private string sender;
        private string senderLocation;
        private string receiver;
        private string receiverLocation;
        private DateTime sentTime;
        private DateTime receivedTime;

        public int Amount
        {
            get { return amount; }
        }

        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public string Sender
        {
            get { return sender; }
            set { sender = value; }
        }

        public string SenderLocation
        {
            get { return senderLocation; }
            set { senderLocation = value; }
        }

        public string Receiver
        {
            get { return receiver; }
            //set { receiver = value; }
        }

        public string ReceiverLocation
        {
            get { return receiverLocation; }
            //set { receiverLocation = value; }
        }

        public DateTime SentTime
        {
            get { return sentTime; }
            set { sentTime = value; }
        }

        public DateTime ReceivedTime
        {
            get { return receivedTime; }
            set { receivedTime = value; }
        }

        public string Message { get; set; }

        /// <summary>
        /// name and location of the receiver (server)
        /// </summary>
        /// <param name="receiver"></param>
        /// <param name="receiverLocation"></param>
        public FrontlineFileReaderLog(string receiver, string receiverLocation)
        {
            this.receiver = receiver;
            this.receiverLocation = receiverLocation;
        }

        /// <summary>
        /// adds a sms count by 1
        /// </summary>
        public void Count()
        {
            amount++;
        }

        /// <summary>
        /// adds a sms count by n
        /// </summary>
        public void Count(int n)
        {
            amount += n;
        }
    }
}
