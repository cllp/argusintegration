﻿using ActionFramework;
using ActionFramework.Base;
using ArgusIntegration.Helpers;
using ArgusIntegration.Logs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ArgusIntegration.Actions
{
    /// <summary>
    /// Action class for reading sms report files
    /// </summary>
    public class FrontlineFileReader : ActionBase
    {
        /// <summary>
        /// the receiving part (server) name
        /// </summary>
        public string Receiver { get; set; }

        /// <summary>
        /// the receiving part (server) location (geo?)
        /// </summary>
        public string ReceiverLocation { get; set; }

        public override object Execute()
        {
            //throw new Exception("This is the exception!!!!");

            try
            {
                //instance of the frontline file reader log
                var fileLog = new FrontlineFileReaderLog(Receiver, ReceiverLocation);

                //get the first resource (picked up from file system watcher)
                var resource = this.Resources.FirstOrDefault();

                if (resource != null)
                {
                    //add info about the file to the log
                    fileLog.FileName = resource.FileName;
                    fileLog.FilePath = resource.FilePath;

                    //file as byte[]
                    var fileBytes = ActionFactory.Compression.DecompressFile(resource.CompressedFile);

                    //filecontent as string
                    string fileContent = resource.FileContent;

                    //return the document to another action
                    XDocument doc = XDocument.Parse(fileContent);

                    //test: write the file content to the log message
                    fileLog.Message = "Parsed filecontent successfully";
                    Log.Custom(fileLog);

                    return doc;
                }
                else
                {
                    fileLog.Message = "No resource file";
                }

                return HandleSuccess();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}
