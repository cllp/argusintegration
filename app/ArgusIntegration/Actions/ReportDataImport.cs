﻿using ActionFramework.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ArgusIntegration.Actions
{
    public class ReportDataImport : ActionBase
    {
        public override object Execute()
        {
            try
            {
                //find the File Reader action 
                var action = this.FindActionByType("FrontlineFileReader");

                //execute the action to get the XDocument File
                XDocument doc = (XDocument)action.Execute();

                //log the commectionstring configuration property
                Log.Info(Prop("ConnectionString"));
                Log.Info(doc.ToString());

                return HandleSuccess();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}
